package util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyReader {

    private static Properties properties;

    static {
        try (InputStream input = new FileInputStream("application.properties")) {
            properties = new Properties();
            // load a properties file
            properties.load(input);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }


    public static String getContractsUrl() {
        return properties.getProperty("contracts_url");
    }
}
