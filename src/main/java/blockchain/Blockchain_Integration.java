package blockchain;

import contract.EmploymentContract;
import dto.EmpContractDto;
import dto.Input;
import dto.Offer;
import org.web3j.crypto.Credentials;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.methods.response.Web3ClientVersion;
import org.web3j.protocol.http.HttpService;
import org.web3j.tx.gas.DefaultGasProvider;
import util.PropertyReader;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Blockchain_Integration {


    private EmploymentContract getEmploymentContract() throws IOException {
        String contracts_url = PropertyReader.getContractsUrl();

        Web3j web3 = Web3j.build(new HttpService(contracts_url));
        Web3ClientVersion web3ClientVersion = web3.web3ClientVersion().send();
        String account = web3.ethAccounts().send().getAccounts().get(0);
        Credentials credentials = Credentials.create(account);
        String contractAddress = "0x345ca3e014aaf5dca488057592ee47305d9b3e10"; //The deployed contract address, taken from truffle console or ganache logs
        BigInteger gasPrice = new BigInteger(String.valueOf(DefaultGasProvider.GAS_PRICE));
        BigInteger gasLimit = new BigInteger(String.valueOf(DefaultGasProvider.GAS_LIMIT));
        return EmploymentContract.load(contractAddress, web3, credentials, gasPrice, gasLimit);
    }

    public List<List<Input>> getAllEmploymentRecords(byte[] address) throws IOException, ClassNotFoundException {

        //Now you can call methods
        byte[][] allEmpRecords = getEmploymentContract().getEmpContracts(address);
        List<EmpContractDto> employmentContractDtoList = new ArrayList<>();
        for (byte[] empRecord : allEmpRecords) {
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(empRecord);
            ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
            Object object = objectInputStream.readObject();
            employmentContractDtoList.add((EmpContractDto) object);
        }

        List<List<Input>> empContractsPublicDetailList = employmentContractDtoList
                .stream()
                .filter(emp -> "publicDetails".equals(emp.getName()))
                .map(empContractDto -> empContractDto.getInputs())
                .collect(Collectors.toList());

        return empContractsPublicDetailList;
    }

}
