package dto;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "constant",
        "inputs",
        "name",
        "outputs",
        "payable",
        "stateMutability",
        "type"
})
public class EmpContractDto {

    @JsonProperty("constant")
    private Boolean constant;
    @JsonProperty("inputs")
    private List<Input> inputs = null;
    @JsonProperty("name")
    private String name;
    @JsonProperty("outputs")
    private List<Output> outputs = null;
    @JsonProperty("payable")
    private Boolean payable;
    @JsonProperty("stateMutability")
    private String stateMutability;
    @JsonProperty("type")
    private String type;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("constant")
    public Boolean getConstant() {
        return constant;
    }

    @JsonProperty("constant")
    public void setConstant(Boolean constant) {
        this.constant = constant;
    }

    @JsonProperty("inputs")
    public List<Input> getInputs() {
        return inputs;
    }

    @JsonProperty("inputs")
    public void setInputs(List<Input> inputs) {
        this.inputs = inputs;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("outputs")
    public List<Output> getOutputs() {
        return outputs;
    }

    @JsonProperty("outputs")
    public void setOutputs(List<Output> outputs) {
        this.outputs = outputs;
    }

    @JsonProperty("payable")
    public Boolean getPayable() {
        return payable;
    }

    @JsonProperty("payable")
    public void setPayable(Boolean payable) {
        this.payable = payable;
    }

    @JsonProperty("stateMutability")
    public String getStateMutability() {
        return stateMutability;
    }

    @JsonProperty("stateMutability")
    public void setStateMutability(String stateMutability) {
        this.stateMutability = stateMutability;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
