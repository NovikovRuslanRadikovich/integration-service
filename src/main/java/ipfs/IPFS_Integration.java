package ipfs;

import io.ipfs.api.IPFS;
import io.ipfs.multihash.Multihash;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class IPFS_Integration extends IPFS {


    public IPFS_Integration(String host, int port) {
        super(host, port);
    }


    public Object fetchUserInfoFromIpfs(IPFS ipfsObject, Multihash hashValue) {

        byte[] retrievedBytes;
        ObjectInputStream objectInputStream;
        Object object;
        try {
            retrievedBytes = ipfsObject.get(hashValue);
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(retrievedBytes);
            objectInputStream = new ObjectInputStream(byteArrayInputStream);
            object = objectInputStream.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }



        return null;
    }

}
