package contract;

import io.ipfs.multihash.Multihash;
import org.web3j.crypto.Credentials;
import org.web3j.protocol.Web3j;
import org.web3j.tx.Contract;
import org.web3j.tx.TransactionManager;
import org.web3j.tx.gas.ContractGasProvider;

import java.math.BigInteger;
import java.util.List;
import java.util.Map;


/**
 * <p>Auto generated code.
 * <p><strong>Do not modify!</strong>
 * <p>Please use the <a href="https://docs.web3j.io/command_line.html">web3j command line tools</a>,
 * or the org.web3j.codegen.SolidityFunctionWrapperGenerator in the
 * <a href="https://github.com/web3j/web3j/tree/master/codegen">codegen module</a> to update.
 *
 * <p>Generated with web3j version 4.1.1.
 */
public class EmploymentContract extends Contract {

    private static final String BINARY = "Bin file was not provided";

    public static final String FUNC_SUPPORTSINTERFACE = "supportsInterface";

    protected EmploymentContract(String contractBinary, String contractAddress, Web3j web3j, TransactionManager transactionManager, ContractGasProvider gasProvider) {
        super(contractBinary, contractAddress, web3j, transactionManager, gasProvider);
    }

    public EmploymentContract(String contractAddress, Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        super(contractAddress, web3j, credentials, gasPrice, gasLimit);
    }

    public EmploymentContract(String contractAddress, Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        super(contractAddress, web3j, transactionManager, gasPrice, gasLimit);
    }

    @Deprecated
    public static EmploymentContract load(String contractAddress, Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        return new EmploymentContract(contractAddress, web3j, credentials, gasPrice, gasLimit);
    }

    @Deprecated
    public static EmploymentContract load(String contractAddress, Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        return new EmploymentContract(contractAddress, web3j, transactionManager, gasPrice, gasLimit);
    }

    private Map<byte[], Multihash> employeeInfo;
    private List<byte[]> employees;
    private Map<byte[],Multihash> orgInfo;
    private List<byte[]> orgs;
    private Map<byte[],byte[][]> empContractsOf;
    private Map<byte[],byte[][]> employeesOf;

    public void newEmployee(byte[] address, Multihash employee) {
        employeeInfo.put(address,employee);
        employees.add(address);
    }

    public void newOrganization(byte[] address, Multihash organization) {
        orgInfo.put(address,organization);
        orgs.add(address);
    }

    public byte[][] getStaff(byte[] organizationAddress) {
        return employeesOf.get(organizationAddress);
    }

    public byte[][] getEmpContracts(byte[] employeeAddress) {
        return empContractsOf.get(employeeAddress);
    }

    public int getEmpContractsCount(byte[] sender) {
        return empContractsOf.get(sender).length;
    }

    public List<byte[]> getEmployees() {
        return employees;
    }

    public List<byte[]> getOrgs() {
        return orgs;
    }
}
